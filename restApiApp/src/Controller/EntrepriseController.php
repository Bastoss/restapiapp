<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Entreprise;
use GuzzleHttp\Client;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;




#[Route('/api', name: 'api_')]
class EntrepriseController extends AbstractController
{
    #[Route('/api-ouverte-ent-liste.php', name: 'entreprise_index')]
    public function index(Request $request, ManagerRegistry $doctrine, SerializerInterface $serializer): Response
    {
        $entreprises = $doctrine
            ->getRepository(Entreprise::class)
            ->findAll();
    
        $data = [];
    
        foreach ($entreprises as $entreprise) {
            $data[] = [
                'id' => $entreprise->getId(),
                'nom' => $entreprise->getNom(),
                'raisonSociale' => $entreprise->getRaisonSociale(),
                'siren' => $entreprise->getSiren(),
                'siret' => $entreprise->getSiret(),
                'adresse' => $entreprise->getAdresse(),
            ];
        }

    
        $requestedFormat = $request->getAcceptableContentTypes();
    
        if ($request->getMethod() !== 'GET') {
            return new Response('Méthode non autorisée', Response::HTTP_METHOD_NOT_ALLOWED);
        }
    
        if (empty($data)) {
            return new Response('Aucune entreprise enregistree', Response::HTTP_OK);
        }
    
        if (in_array('application/json', $requestedFormat)) {
            $jsonContent = json_encode($data);
            $response = new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
            $response->headers->set('Status-Text', 'Liste des entreprises presentes (JSON)');
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } elseif (in_array('text/csv', $requestedFormat)) {
            $filename = 'entreprise_data.csv';
    
            $response = new StreamedResponse(function () use ($serializer, $data) {
                $csvData = $serializer->serialize($data, CsvEncoder::FORMAT, [
                    CsvEncoder::DELIMITER_KEY => ';',
                    AbstractNormalizer::IGNORED_ATTRIBUTES => ['internalAttribute'],
                ]);
    
                $outputStream = fopen('php://output', 'w');
                fwrite($outputStream, $csvData);
                fclose($outputStream);
            });
    
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
            $response->headers->set('Status-Text', 'Liste des entreprises presentes (CSV)');
            return $response;
        } else {
            return new Response('Format non pris en compte', Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    #[Route('/entreprises', name: 'entreprise_download')]
    public function download(ManagerRegistry $doctrine, Request $request): JsonResponse
    {


        $entityManager = $doctrine->getManager();
        $data = json_decode($request->getContent(), true);


        if ($request->getMethod() !== 'POST') {
            return new JsonResponse(['error' => 'Methode non autorisee'],  JsonResponse::HTTP_METHOD_NOT_ALLOWED);
        }

        $insertedCompanies = [];
        $entreprise = new Entreprise();

        $entreprise->setRaisonSociale($data['raisonSociale']);
        $entreprise->setSiren($data['siren']);
        $entreprise->setSiret($data['siret']);
        $entreprise->setAdresse($data['adresse']);
        $entreprise->setNom($data['nom']);
        
        $entityManager->persist($entreprise);

        $insertedCompanies[] = [
            'nom' => $entreprise->getNom(),
            'raisonSociale' => $entreprise->getRaisonSociale(),
            'siren' => $entreprise->getSiren(),
            'siret' => $entreprise->getSiret(),
            'adresse' => $entreprise->getAdresse(),
        ];


        $entityManager->flush();

        $jsonContent = json_encode($insertedCompanies);
        $response = new JsonResponse($jsonContent, 201, [], true);
        $response->headers->set('Status-Text', 'Entreprise telechargee en BDD (HTTP 201)');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    #[Route('/api-ouverte-entreprise', name: 'entreprise_create')]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $data = json_decode($request->getContent(), true);

        if ($request->getMethod() !== 'POST') {
            return new JsonResponse(['error' => 'Methode non autorisee'],  JsonResponse::HTTP_METHOD_NOT_ALLOWED);
        }

        $requiredKeys = ['siren', 'raison_sociale', 'adresse.num', 'adresse.voie', 'adresse.code_postale', 'adresse.ville', 'adresse.gps.latitude', 'adresse.gps.longitude'];

        $missingKeys = [];

        foreach ($requiredKeys as $key) {
            $keys = explode('.', $key);
            $currentData = $data;

            foreach ($keys as $nestedKey) {
                if (!isset($currentData[$nestedKey])) {
                    $missingKeys[] = $key;
                    break;
                }

                $currentData = $currentData[$nestedKey];
            }
        }

        if (!empty($missingKeys)) {
            return new JsonResponse(['error' => 'Format JSON invalide'], 400);
        } 

        if (!$data['siren']  || !$data['raison_sociale'] || !$data['adresse']['num'] || !$data['adresse']['voie'] || !$data['adresse']['ville'] || !$data['adresse']['code_postale']) {
            return new JsonResponse(['error' => 'Donnee manquante (HTTP 400)'], 400);
        }


        $entreprise = $doctrine->getRepository(Entreprise::class)->findOneBy(['siren' => $data['siren']]);

        if($entreprise != null){
            return new JsonResponse(['error' => 'Entreprise existe deja (HTTP 409)'], 409);
        }


        $insertedCompanies = [];


        $entreprise = new Entreprise();

        $adresse = $data['adresse']['num'] . " " .  $data['adresse']['voie'] . ", " . $data['adresse']['ville'] . " " . $data['adresse']['code_postale'];


        $entreprise->setNom($data['raison_sociale']);
        $entreprise->setRaisonSociale($data['raison_sociale']);
        $entreprise->setSiren($data['siren']);
        $entreprise->setSiret($data['siren']);
        $entreprise->setAdresse($adresse);
        
        $entityManager->persist($entreprise);

        $insertedCompanies[] = [
            'nom' => $entreprise->getNom(),
            'raisonSociale' => $entreprise->getRaisonSociale(),
            'siren' => $entreprise->getSiren(),
            'siret' => $entreprise->getSiret(),
            'adresse' => $entreprise->getAdresse(),
        ];


        $entityManager->flush();

        $jsonContent = json_encode($insertedCompanies);
        $response = new JsonResponse($jsonContent, 201, [], true);
        $response->headers->set('Status-Text', 'Entreprise creee (HTTP 201)');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    #[Route('/api-ouverte-ent.php', name: 'entreprise_show')]
    public function show(Request $request,ManagerRegistry $doctrine): JsonResponse
    {
        if ($request->getMethod() !== 'GET') {
            return new JsonResponse(['error' => 'Méthode non autorisée'],  JsonResponse::HTTP_METHOD_NOT_ALLOWED);
        }

        $siren = $request->query->get('siren');

        if (!$siren) {
            return new JsonResponse(['error' => 'Le paramètre siren est manquant.'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $entreprise = $doctrine->getRepository(Entreprise::class)->findOneBy(['siren' => $siren]);

        if (!$entreprise) {
            return new JsonResponse(['error' => 'Aucune entreprise avec ce SIREN (HTTP 404).'], JsonResponse::HTTP_NOT_FOUND);
        }

        $data =  [
            'id' => $entreprise->getId(),
            'nom' => $entreprise->getNom(),
            'raisonSociale' => $entreprise->getRaisonSociale(),
            'siren' => $entreprise->getSiren(),
            'siret' => $entreprise->getSiret(),
            'adresse' => $entreprise->getAdresse(),
        ];

        $jsonContent = json_encode($data);
        $response = new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
        $response->headers->set('Status-Text', 'Informations au format JSON');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    #[Route('/apientreprises', name: 'entreprise_api_show', methods: ['get'])]
    public function showApiEntreprise(Request $request): JsonResponse
    {
        $nom = $request->query->get("nom");

        // Créer l'URL avec les paramètres
        $url = 'https://recherche-entreprises.api.gouv.fr/search?q=' . $nom;
        $client = new Client();
        $response = $client->request('GET', $url);
        $entreprises = json_decode($response->getBody(), true);
        // echo ($entreprises);
        $return = [];

        foreach ($entreprises as $entreprise) {
            // dump($entreprise);
            if (is_array($entreprise)) {
                foreach ($entreprise as $sousEntreprises) {
                    // dump($sousEntreprises["nom_complet"]);
                    $return[] = [
                        'nom' => $sousEntreprises["nom_complet"],
                        'raisonSociale' => $sousEntreprises["nom_raison_sociale"],
                        'siren' => $sousEntreprises["siren"],
                        'siret' => $sousEntreprises["siege"]["siret"],
                        'adresse' => $sousEntreprises["siege"]["adresse"]
                    ];
                }
            }
        }

        return $this->json($return);
    }

    #[Route('/entreprises/{id}', name: 'entreprise_update', methods: ['put', 'patch'])]
    public function update(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);

        if (!$entreprise) {
            return $this->json('No entreprise found for id' . $id, 404);
        }

        $data = json_decode($request->getContent(), true);

        $entreprise->setNom($data['nom']);
        $entreprise->setRaisonSociale($data['raisonSociale']);
        $entreprise->setSiren($data['siren']);
        $entreprise->setSiret($data['siret']);
        $entreprise->setAdresse($data['adresse']);
        $entityManager->flush();

        $data =  [
            'id' => $entreprise->getId(),
            'nom' => $entreprise->getNom(),
            'raisonSociale' => $entreprise->getRaisonSociale(),
            'siren' => $entreprise->getSiren(),
            'siret' => $entreprise->getSiret(),
            'adresse' => $entreprise->getAdresse(),
        ];

        return $this->json($data);
    }

    #[Route('/entreprises/{id}', name: 'entreprise_delete', methods: ['delete'])]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);

        if (!$entreprise) {
            return $this->json('No entreprise found for id' . $id, 404);
        }

        $entityManager->remove($entreprise);
        $entityManager->flush();

        return $this->json('Deleted a entreprise successfully with id ' . $id);
    }


}
