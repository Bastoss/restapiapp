<?php

namespace App\Controller;

use App\Service\UrsafService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;


#[Route('/ursaf', name: 'ursaf_')]
class CalculUrsafController extends AbstractController
{
    #[Route('/syntheseentreprise', name: 'entreprise_create', methods: ['post'])]
    public function create(UrsafService $ursafService, ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);


        $situation = $data['situation'];
        $expressions = $data['expressions'];
        // dd($situation["salarié . contrat"]);

        if ($situation["salarié . contrat"] == "'CDI'"){
            $retour = $ursafService->salaireNetCdiAvantImpotEtStage($data);

        }

        if ($situation["salarié . contrat"] == "'CDD'"){
            $retour = $ursafService->salaireNetCddAvantImpotEtStage($data);

        }

        if ($situation["salarié . contrat"] == "'apprentissage'"){
            $retour = $ursafService->salaireNetAlternantAvantImpotEtStage($data);

        }

        return $this->json($retour);
    }
}