<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Compte;
use App\Entity\Entreprise;
use GuzzleHttp\Client;

use Doctrine\ORM\EntityManagerInterface;


use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;





#[Route('/compte', name: 'compte_')]
class CompteController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    private EventDispatcherInterface $dispatcher;
    private TokenStorageInterface $tokenStorage;

    public function __construct(EntityManagerInterface $entityManager,EventDispatcherInterface $dispatcher, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
        $this->tokenStorage = $tokenStorage;
    }

    #[Route('/login', name: 'compte_login')]
    public function login(Request $request, Security $security)
    {
        $authorizationHeader = $request->headers->get('Authorization');

        if (!$authorizationHeader || strpos($authorizationHeader, 'Basic ') !== 0) {
            return new JsonResponse(['error' => 'En-Tête Authorization invalide.'], 406);
        }

        // Décoder l'en-tête d'autorisation HTTP Basic
        $credentials = base64_decode(substr($authorizationHeader, 6));

        if ($credentials === false) {
            return new JsonResponse(['error' => 'Encodement en base64 invalide.'], 402);
        }

        // Extraire le nom d'utilisateur et le mot de passe
        [$username, $password] = explode(':', $credentials, 2);

        // Vérifier les informations d'identification
        $user = $this->entityManager->getRepository('App\Entity\Compte')->findOneBy(['username' => $username]);

        if (!$user || !password_verify($password, $user->getPassword())) {
            // Utilisateur ou mot de passe incorrect, renvoyer une réponse avec le code HTTP 401
            return new JsonResponse(['error' => 'Utilisateur ou mot de passe incorrect'], 401);
        }


        
        // Authentification réussie - connectez l'utilisateur
        $token = new UsernamePasswordToken($user, $user->getPassword(), $user->getRoles());
        $this->tokenStorage->setToken($token);

        $security2 = $this->container->get('security.token_storage');
        $security2->setToken($token);

        // Déclencher l'événement de connexion
        $event = new InteractiveLoginEvent($request, $token);
        $this->dispatcher->dispatch($event, 'security.interactive_login');

        // Réponse JSON pour indiquer la réussite de la connexion
        return new JsonResponse(['success' => true, 'username' => $this->tokenStorage->getToken()->getUser()->getUsername()]);
    }

    #[Route('/create_account.php', name: 'compte_create')]
    public function createAccount(ManagerRegistry $doctrine,Request $request): Response
    {

        $entityManager = $doctrine->getManager();


        $data = json_decode($request->getContent(), true);
        $username = $data['username'];
        $password = $data['password'];

        $compte = new Compte();

        $compte->setUsername($username);
        $compte->setPassword($password);
        
        $entityManager->persist($compte);
        $entityManager->flush();
        
       

        return new JsonResponse(['message' => 'Compte créé avec succès'], Response::HTTP_CREATED);
    }

    
    #[Route('/test', name: 'test')]
    public function test(Request $request, Security $security)
    {
        $token = $this->tokenStorage->getToken()->getUser()->getId();

        return new JsonResponse(['success' =>  $token]);
    }

    #[Route('/api-protege', name: 'api_protege')]
    public function protectedAction(Request $request,  Security $security): Response
    {

        $security2 = $this->container->get('security.token_storage');

        if (!$security2 && !$security2->getToken()) {
            return new JsonResponse(['error' => 'Non authentifié'],  401);
        }


        if ($request->getMethod() !== "PATCH" && $request->getMethod() !== "DELETE") {
            // dd($request->getMethod() !== "PATCH");
            return new JsonResponse(['error' => 'Methode non autorisee'],  JsonResponse::HTTP_METHOD_NOT_ALLOWED);
        }

        

        if ($request->getMethod() == "PATCH" ) {
           
            $data = json_decode($request->getContent(), true);

            if (!isset($data['siren'])) {
                return new JsonResponse(['error' => 'Format JSON invalide (HTTP 400)'], JsonResponse::HTTP_BAD_REQUEST);
            }

            $entreprise = $this->entityManager->getRepository(Entreprise::class)->findOneBy(['siren' => $data['siren']]);

            if (!$entreprise) {
                return new JsonResponse(['error' => 'Aucune entreprise avec ce SIREN (HTTP 404)'], JsonResponse::HTTP_NOT_FOUND);
            }

            if (isset($data['nom'])) {
                $entreprise->setNom($data['nom']);
            }

            if (isset($data['raison_sociale'])) {
                $entreprise->setRaisonSociale($data['raison_sociale']);
            }

            if (isset($data['siret'])) {
                $entreprise->setSiret($data['siret']);
            }

            if (isset($data['adresse'])) {
                $entreprise->setAdresse($data['adresse']);
            }

           
            $this->entityManager->persist($entreprise);
            $this->entityManager->flush();

            $response = new JsonResponse("Entreprise modifiée (HTTP 200)", 200, [], true);
            $response->headers->set('Status-Text', 'Entreprise modifiée (HTTP 200)');
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

        else if ($request->getMethod() == "DELETE" ) {
           
            $data = json_decode($request->getContent(), true);

            

            if (!isset($data['siren'])) {
                return new JsonResponse(['error' => 'Format JSON invalide (HTTP 400)'], JsonResponse::HTTP_BAD_REQUEST);
            }
            $entreprise = $this->entityManager->getRepository(Entreprise::class)->findOneBy(['siren' => $data['siren']]);

            if (!$entreprise) {
                return new JsonResponse(['error' => 'Aucune entreprise avec ce SIREN (HTTP 404)'], JsonResponse::HTTP_NOT_FOUND);
            }

            
           
            $this->entityManager->remove($entreprise);
            $this->entityManager->flush();

            $response = new JsonResponse("Entreprise supprimée (HTTP 200)", 200, [], true);
            $response->headers->set('Status-Text', 'Entreprise supprimée (HTTP 200)');
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

        return new Response('Action protégée réussie');
    }

    #[Route('/deconnexion', name: 'deconnexion')]
    public function logout()
    {
        $this->tokenStorage->setToken(null);
        return new JsonResponse(['message' => 'Déconnexion réussie'], Response::HTTP_OK);
    }
}