<?php

namespace App\Service;

use GuzzleHttp\Client;

class UrsafService
{

    public function salaireNetCdiAvantImpotEtStage($body)
    {
        $retourAPi = $this->callUrsaf($body);

        $response = [
            "salaireNetCdi" => $retourAPi[0],
            "coutEmployeurCdi" => $retourAPi[1],
            "cotisationCdi" => $retourAPi[2],
            "gratificationMinimaleStage" => $retourAPi[3]
        ];

        return $response;
    }

    public function salaireNetCddAvantImpotEtStage($body)
    {
        $retourAPi = $this->callUrsaf($body);

        $response = [
            "salaireNetCdd" => $retourAPi[0],
            "coutEmployeurCdd" => $retourAPi[1],
            "cotisationCdd" => $retourAPi[2],
            "gratificationMinimaleStage" => $retourAPi[3]
        ];


        return $response;
    }

    public function salaireNetAlternantAvantImpotEtStage($body)
    {
        $retourAPi = $this->callUrsaf($body);

        $response = [
            "salaireNetAlternant" => $retourAPi[0],
            "coutEmployeurAlternant" => $retourAPi[1],
            "cotisationAlternant" => $retourAPi[2],
            "gratificationMinimaleStage" => $retourAPi[3]
        ];


        return $response;
    }

    public function callUrsaf($body){
        $url = 'https://mon-entreprise.urssaf.fr/api/v1/evaluate';
        $client = new Client();
        $response = $client->request('POST', $url, [
            'json' => $body,
        ]);

        $result = json_decode($response->getBody(), true);
        $salaireNet = $result["evaluate"][0]["nodeValue"];
        $coutEmployeur = $result["evaluate"][1]["nodeValue"];
        $cotisation = $result["evaluate"][2]["nodeValue"];
        $stage = 15/100*($result["evaluate"][3]["nodeValue"]);

        $response = [
            $salaireNet,
            $coutEmployeur,
            $cotisation,
            $stage
        ];

        return $response;
    }
}