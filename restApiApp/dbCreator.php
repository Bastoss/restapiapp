<?php
$servername = "localhost"; // conf bastien
// $servername = "localhost:8889"; // conf mohammed
$username = "root"; // Remplacez par votre nom d'utilisateur MySQL
$password = "root"; // Remplacez par votre mot de passe MySQL

// Connexion à MySQL
$conn = new mysqli($servername, $username, $password);

// Vérification de la connexion
if ($conn->connect_error) {
    die("Échec de la connexion : " . $conn->connect_error);
}

// Création de la base de données
$sql = "CREATE DATABASE restapiapp";
if ($conn->query($sql) === TRUE) {
    echo "Base de données créée avec succès.";
} else {
    echo "Erreur lors de la création de la base de données : " . $conn->error;
}

// Fermeture de la connexion
$conn->close();