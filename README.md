# RestApiAPP

## Getting started

pour le backend !!! PREREQUIS : avoir symfony command line installé sur sa machine !!!

```
cd restApiApp
composer install
php dbCreator.php
php bin/console make:migration
php bin/console doctrine:migrations:migrate
symfony server:start

NE PAS OUBLIER DE MODIFIER LE .ENV ET LE DBCREATOR
```

pour le frontend

```
cd mon-app
npm install
npm start
```
