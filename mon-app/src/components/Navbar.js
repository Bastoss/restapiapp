import React, { useState } from "react";
import axios from "axios";

const Navbar = ({
  sendOpen,
  estConnectee,
  setEstConnectee,
  setOuvrirSuppr,
  setOuvrirModif,
}) => {
  const sendOpenParent = () => {
    sendOpen(true);
  };

  const ouvrirModif = () => {
    setOuvrirModif(true);
  };

  const ouvrirSuppr = () => {
    setOuvrirSuppr(true);
  };

  const deconnexion = () => {
    const session_url = "http://127.0.0.1:8000/compte/deconnexion";

    var config = {
      method: "post",
      url: session_url,
    };

    axios(config)
      .then(function (response) {
        localStorage.removeItem("estConnectee");
        setEstConnectee(false);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="navbar shadow-md bg-slate-50 w-11/12 rounded-lg mt-10 mb-10">
      <div className="navbar-start">
        <p className="text-2xl pl-10 ont-extrabold text-transparent bg-clip-text bg-gradient-to-r from-blue-400 to-red-400">
          MohBast
        </p>
      </div>
      <div className="navbar-center hidden lg:flex">
        <ul className="menu menu-horizontal px-1 ">
          <li>
            <a href="/">Exercice 1</a>
          </li>
          <li>
            <a href="/about">Exercice 2 et 3</a>
          </li>
        </ul>
      </div>

      <div className="navbar-end">
        {estConnectee ? (
          <div className=" flex gap-1">
            <a onClick={ouvrirModif} className="btn bg-green-300 text-sm">
              Modifier entreprise
            </a>
            <a onClick={ouvrirSuppr} className="btn bg-red-400">
              Supprimer entreprise
            </a>
            <a onClick={deconnexion} className="btn bg-slate-300">
              Déconnexion
            </a>
          </div>
        ) : (
          <a onClick={sendOpenParent} className="btn">
            Se connecter
          </a>
        )}
      </div>
    </div>
  );
};

export default Navbar;
