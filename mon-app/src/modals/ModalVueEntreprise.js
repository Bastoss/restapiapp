import Modal from "@mui/joy/Modal";
import ModalClose from "@mui/joy/ModalClose";
import Sheet from "@mui/joy/Sheet";
import Typography from "@mui/joy/Typography";
import Input from "@mui/joy/Input";
import Button from "@mui/joy/Button";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useData } from "../context/DataContext";

const ModalVueEntreprise = ({
  ouvrirModif,
  setOuvrirModif,
  entreprise,
  appelApiEntreprise,
}) => {
  const [data, setData] = useState([]);
  const [salaireCDI, setSalaireCDI] = useState("");
  const [salaireCDD, setSalaireCDD] = useState("");
  const [salaireAlternant, setSalaireAlternant] = useState("");
  const [valeurAfficheeCDI, setValeurAfficheeCDI] = useState("");
  const [valeurAfficheeCDD, setValeurAfficheeCDD] = useState("");
  const [valeurAfficheeAlternant, setValeurAfficheeAlternant] = useState("");
  const [valeurAfficheeCoutCDD, setValeurAfficheeCoutCDD] = useState("");
  const [valeurAfficheeCoutCDI, setValeurAfficheeCoutCDI] = useState("");
  const [valeurAfficheeCotisationCDD, setValeurAfficheeCotisationCDD] =
    useState("");
  const [valeurAfficheeCotisationCDI, setValeurAfficheeCotisationCDI] =
    useState("");
  const [valeurAfficheeCoutAlternant, setValeurAfficheeCoutAlternant] =
    useState("");
  const [
    valeurAfficheeCotisationAlternant,
    setValeurAfficheeCotisationAlternant,
  ] = useState("");
  const [
    valeurAfficheeGratificationStage,
    setValeurAfficheeGratificationStage,
  ] = useState("");
  const [error, setError] = useState(null);
  const [modalOpened, setModalOpened] = useState(false);
  const { updateData } = useData();

  useEffect(() => {
    if (appelApiEntreprise == true) {
      const fetchData = async () => {
        const requestBody = {
          situation: {
            "salarié . contrat . salaire brut": {
              valeur: 0,
              unité: "€ / mois",
            },
            "salarié . contrat": "'apprentissage'",
          },
          expressions: [
            "salarié . rémunération . net . à payer avant impôt",
            "salarié . coût total employeur",
            "salarié . cotisations",
            "plafond sécurité sociale",
          ],
        };

        try {
          const response = await axios.post(
            "http://localhost:8000/ursaf/syntheseentreprise",
            requestBody
          );
          setValeurAfficheeGratificationStage(
            response.data["gratificationMinimaleStage"]
          );
        } catch (error) {
          setError(error);
        }
      };

      fetchData();
    }
  }, [modalOpened]);

  if (error) {
    return <p>Une erreur s'est produite : {error.message}</p>;
  }

  const onclickCDI = async () => {
    const requestBody = {
      situation: {
        "salarié . contrat . salaire brut": {
          valeur: salaireCDI,
          unité: "€ / mois",
        },
        "salarié . contrat": "'CDI'",
      },
      expressions: [
        "salarié . rémunération . net . à payer avant impôt",
        "salarié . coût total employeur",
        "salarié . cotisations",
        "plafond sécurité sociale",
      ],
    };

    try {
      const response = await axios.post(
        "http://localhost:8000/ursaf/syntheseentreprise",
        requestBody,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log(response.data);
      setValeurAfficheeGratificationStage(
        response.data["gratificationMinimaleStage"]
      );
      setValeurAfficheeCDI(response.data["salaireNetCdi"]);
      setValeurAfficheeCoutCDI(response.data["coutEmployeurCdi"]);
      setValeurAfficheeCotisationCDI(response.data["cotisationCdi"]);
    } catch (error) {}
  };

  const onclickCDD = async () => {
    const requestBody = {
      situation: {
        "salarié . contrat . salaire brut": {
          valeur: salaireCDD,
          unité: "€ / mois",
        },
        "salarié . contrat": "'CDD'",
      },
      expressions: [
        "salarié . rémunération . net . à payer avant impôt",
        "salarié . coût total employeur",
        "salarié . cotisations",
        "plafond sécurité sociale",
      ],
    };

    try {
      const response = await axios.post(
        "http://localhost:8000/ursaf/syntheseentreprise",
        requestBody,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log(response.data);
      setValeurAfficheeGratificationStage(
        response.data["gratificationMinimaleStage"]
      );
      setValeurAfficheeCDD(response.data["salaireNetCdd"]);
      setValeurAfficheeCoutCDD(response.data["coutEmployeurCdd"]);
      setValeurAfficheeCotisationCDD(response.data["cotisationCdd"]);
    } catch (error) {}
  };

  const onclickAlternant = async () => {
    const requestBody = {
      situation: {
        "salarié . contrat . salaire brut": {
          valeur: salaireAlternant,
          unité: "€ / mois",
        },
        "salarié . contrat": "'apprentissage'",
      },
      expressions: [
        "salarié . rémunération . net . à payer avant impôt",
        "salarié . coût total employeur",
        "salarié . cotisations",
        "plafond sécurité sociale",
      ],
    };

    try {
      const response = await axios.post(
        "http://localhost:8000/ursaf/syntheseentreprise",
        requestBody,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log(response.data);
      setValeurAfficheeGratificationStage(
        response.data["gratificationMinimaleStage"]
      );
      setValeurAfficheeAlternant(response.data["salaireNetAlternant"]);
      setValeurAfficheeCoutAlternant(response.data["coutEmployeurAlternant"]);
      setValeurAfficheeCotisationAlternant(
        response.data["cotisationAlternant"]
      );
    } catch (error) {}
  };

  return (
    <Modal
      aria-labelledby="modal-title"
      aria-describedby="modal-desc"
      open={ouvrirModif}
      onClose={() => {
        setOuvrirModif(false);
        setModalOpened(false);
      }}
      sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
    >
      <Sheet
        variant="outlined"
        sx={{
          width: 1000,
          borderRadius: "md",
          p: 3,
          boxShadow: "lg",
        }}
      >
        <ModalClose variant="plain" sx={{ m: 1 }} />
        <Typography
          component="h2"
          id="modal-title"
          level="h4"
          textColor="inherit"
          fontWeight="lg"
          mb={1}
        >
          Modifier une entreprise
        </Typography>
        <div className="flex flex-col items-center gap-4 mt-10">
          <p>Nom : {entreprise.nom}</p>
          <p>Raison sociale : {entreprise.raisonSociale}</p>
          <p>Adresse : {entreprise.adresse}</p>
          <p>Siren : {entreprise.siren}</p>
          <p>Siren : {entreprise.siret}</p>

          {/* Section Gratification minimale stage */}
          <div>
            <label>Gratification minimale stagiaire</label>
            <Input
              type="text"
              value={valeurAfficheeGratificationStage}
              readOnly
              disabled
            />
          </div>

          {/* Section Salaire CDI */}
          <div className="flex flex-col" id="salaireCDI">
            <div style={{ display: "flex", alignItems: "center", gap: "8px" }}>
              <label htmlFor="salaireCDI">Salaire brut CDI:</label>
              <Input
                type="text"
                id="salaireCDI"
                value={salaireCDI}
                onChange={(e) => setSalaireCDI(e.target.value)}
              />
              <Button onClick={onclickCDI}>Envoyer</Button>
            </div>

            <div id="chargecdi" style={{ display: "flex" }}>
              <div>
                <label>Salaire net CDI</label>
                <Input
                  type="text"
                  value={valeurAfficheeCDI}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cout employeur CDI</label>
                <Input
                  type="text"
                  value={valeurAfficheeCoutCDI}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cotisation CDI</label>
                <Input
                  type="text"
                  value={valeurAfficheeCotisationCDI}
                  readOnly
                  disabled
                />
              </div>
            </div>
          </div>

          <br />

          {/* Section Salaire CDD */}
          <div className="flex flex-col" id="salaireCDD">
            <div style={{ display: "flex", alignItems: "center", gap: "8px" }}>
              <label htmlFor="salaireCDD">Salaire brut CDD:</label>
              <Input
                type="text"
                id="salaireCDD"
                value={salaireCDD}
                onChange={(e) => setSalaireCDD(e.target.value)}
              />
              <Button onClick={onclickCDD}>Envoyer</Button>
            </div>

            <div id="chargecdi" style={{ display: "flex" }}>
              <div>
                <label>Salaire net CDD</label>
                <Input
                  type="text"
                  value={valeurAfficheeCDD}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cout employeur CDD</label>
                <Input
                  type="text"
                  value={valeurAfficheeCoutCDD}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cotisation CDD</label>
                <Input
                  type="text"
                  value={valeurAfficheeCotisationCDD}
                  readOnly
                  disabled
                />
              </div>
            </div>
          </div>

          <br />

          {/* Section Salaire Alternant */}
          <div className="flex flex-col" id="salaireAlternant">
            <div style={{ display: "flex", alignItems: "center", gap: "8px" }}>
              <label htmlFor="salaireAlternant">Salaire brut Alternant:</label>
              <Input
                type="text"
                id="salaireAlternant"
                value={salaireAlternant}
                onChange={(e) => setSalaireAlternant(e.target.value)}
              />
              <Button onClick={onclickAlternant}>Envoyer</Button>
            </div>

            <div id="chargecdi" style={{ display: "flex" }}>
              <div>
                <label>Salaire net Alternant</label>
                <Input
                  type="text"
                  value={valeurAfficheeAlternant}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cout employeur Alternant</label>
                <Input
                  type="text"
                  value={valeurAfficheeCoutAlternant}
                  readOnly
                  disabled
                />
              </div>
              <div>
                <label>Cotisation Alternant</label>
                <Input
                  type="text"
                  value={valeurAfficheeCotisationAlternant}
                  readOnly
                  disabled
                />
              </div>
            </div>
          </div>
        </div>
      </Sheet>
    </Modal>
  );
};

export default ModalVueEntreprise;
