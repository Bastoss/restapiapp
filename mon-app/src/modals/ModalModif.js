import Modal from "@mui/joy/Modal";
import ModalClose from "@mui/joy/ModalClose";
import Sheet from "@mui/joy/Sheet";
import Typography from "@mui/joy/Typography";
import Input from "@mui/joy/Input";
import Button from "@mui/joy/Button";
import Select from "@mui/joy/Select";
import Option from "@mui/joy/Option";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useData } from "../context/DataContext";

const ModalModif = ({ setOuvrirModif, ouvrirModif }) => {
  const [data, setData] = useState([]);
  const [selectedValue, setSelectedValue] = useState("");
  const [sirenEnCours, setSirenEnCours] = useState("");
  const { updateData } = useData();

  useEffect(() => {
    console.log("test");
    axios
      .get("http://127.0.0.1:8000/api/api-ouverte-ent-liste.php")
      .then((data) => {
        setData(data?.data);
        console.log("test2");
        console.log(data);
      })
      .catch(function (error) {
        setData([]);
        console.log(error);
      });
  }, []);

  const saveModif = (event) => {
    console.log(
      event.target.parentElement.parentElement.getElementsByTagName("input")
    );
    const siren =
      event.target.parentElement.parentElement.getElementsByTagName("input")[0]
        .value;
    const raison_sociale =
      event.target.parentElement.parentElement.getElementsByTagName("input")[2]
        .value;
    const adresse =
      event.target.parentElement.parentElement.getElementsByTagName("input")[3]
        .value;
    const siret =
      event.target.parentElement.parentElement.getElementsByTagName("input")[4]
        .value;
    const nom =
      event.target.parentElement.parentElement.getElementsByTagName("input")[5]
        .value;

    const object = {
      siren: siren,
      raison_sociale: raison_sociale,
      adresse: adresse,
      siret: siret,
      nom: nom,
    };

    axios
      .patch("http://127.0.0.1:8000/compte/api-protege", object)
      .then(function (response) {
        setOuvrirModif(false);
        updateData();
        setSirenEnCours("");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleChange = (event) => {
    const siren =
      event.target.parentElement.parentElement.getElementsByTagName("input")[3]
        .value;
    console.log(
      event.target.parentElement.parentElement.getElementsByTagName("input")
    );
    let newData = "";
    data.map((uneData) => {
      if (uneData.siren === siren) {
        newData = uneData;
      }
    });

    event.target.parentElement.parentElement.getElementsByTagName(
      "input"
    )[4].value = newData.siren;
    event.target.parentElement.parentElement.getElementsByTagName(
      "input"
    )[5].value = newData.raisonSociale;
    event.target.parentElement.parentElement.getElementsByTagName(
      "input"
    )[6].value = newData.adresse;
    event.target.parentElement.parentElement.getElementsByTagName(
      "input"
    )[7].value = newData.siret;
    event.target.parentElement.parentElement.getElementsByTagName(
      "input"
    )[8].value = newData.nom;

    setSirenEnCours(newData.siren);
  };

  return (
    <Modal
      aria-labelledby="modal-title"
      aria-describedby="modal-desc"
      open={ouvrirModif}
      onClose={() => setOuvrirModif(false)}
      sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
    >
      <Sheet
        variant="outlined"
        sx={{
          width: 500,
          borderRadius: "md",
          p: 3,
          boxShadow: "lg",
        }}
      >
        <ModalClose variant="plain" sx={{ m: 1 }} />
        <Typography
          component="h2"
          id="modal-title"
          level="h4"
          textColor="inherit"
          fontWeight="lg"
          mb={1}
        >
          Modifier une entreprise
        </Typography>
        <div className="flex flex-col items-center gap-4 mt-10">
          <Select
            color="primary"
            placeholder="Sélectionner une entreprise"
            onChange={(e) => handleChange(e)}
            size="md"
            variant="outlined"
          >
            <Option disabled value="">
              <em>Sélectionner une entreprise</em>
            </Option>
            {Array.isArray(data) &&
              data.length > 0 &&
              data.map((uneData, index) => (
                <Option key={index} value={uneData.siren}>
                  {uneData.nom}
                </Option>
              ))}
          </Select>
          <Input
            disabled
            className="w-11/12"
            color="primary"
            placeholder="SIREN"
            size="lg"
            variant="outlined"
          />
          <Input
            className="w-11/12"
            color="primary"
            placeholder="Raison sociale"
            size="lg"
            variant="outlined"
          />
          <Input
            className="w-11/12"
            color="primary"
            placeholder="Adresse"
            size="lg"
            variant="outlined"
          />
          <Input
            className="w-11/12"
            color="primary"
            placeholder="SIRET"
            size="lg"
            variant="outlined"
          />
          <Input
            className="w-11/12"
            color="primary"
            placeholder="Nom"
            size="lg"
            variant="outlined"
          />
          <Button
            onClick={(e) => saveModif(e)}
            color="primary"
            variant="outlined"
          >
            Enregistrer les modifications
          </Button>
        </div>
      </Sheet>
    </Modal>
  );
};

export default ModalModif;
