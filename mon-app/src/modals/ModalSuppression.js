import Modal from '@mui/joy/Modal';
import ModalClose from '@mui/joy/ModalClose';
import Sheet from '@mui/joy/Sheet';
import Typography from '@mui/joy/Typography';
import Input from '@mui/joy/Input';
import Button from '@mui/joy/Button';
import Select from '@mui/joy/Select';
import Option from '@mui/joy/Option';
import React, {useEffect, useState } from 'react';
import axios from 'axios';
import { useData } from '../context/DataContext';



const ModalSuppression = ({ setOuvrirSuppr, ouvrirSuppr}) => {


    const [data,setData] = useState([]);
    const [selectedValue, setSelectedValue] = useState('');
    const [sirenEnCours, setSirenEnCours] = useState('');
    const { updateData } = useData();

    useEffect(() => {
      console.log("test");
    axios.get("http://127.0.0.1:8000/api/api-ouverte-ent-liste.php").then((data) => {
      setData(data?.data);
      console.log("test2");
      console.log(data);
    }).catch(function (error) {
      setData([]);
      console.log(error)
    });
  },[]);

  const saveSuppr= (event) => {

    console.log({
        siren: sirenEnCours,
      });


    axios({
    method: 'delete',
    url: 'http://127.0.0.1:8000/compte/api-protege',
    data: {
      siren: sirenEnCours,
    },
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      setOuvrirSuppr(false);
      updateData();
      setSirenEnCours('');
    })
    .catch(function (error) {
      console.log(error);
    });

   



  }

  const handleChange = (event) => {
    const siren = event.target.parentElement.parentElement.getElementsByTagName('input')[1].value;
    console.log(siren);
    setSirenEnCours(siren);
  }

return (
    <Modal
            aria-labelledby="modal-title"
            aria-describedby="modal-desc"
            open={ouvrirSuppr}
            onClose={() => setOuvrirSuppr(false)}
            sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
        >
        <Sheet
          variant="outlined"
          sx={{
            width: 500,
            borderRadius: 'md',
            p: 3,
            boxShadow: 'lg',
          }}
        >
          <ModalClose variant="plain" sx={{ m: 1 }} />
          <Typography
            component="h2"
            id="modal-title"
            level="h4"
            textColor="inherit"
            fontWeight="lg"
            mb={1}
          >
           Supprimer une entreprise
          </Typography>
          <div className='flex flex-col items-center gap-4 mt-10'>
          <Select
            color="primary"
            placeholder="Sélectionner une entreprise"
            onChange={(e) => handleChange(e)}
            size="md"
            variant="outlined"
          >
            <Option disabled value="">
              <em>Sélectionner une entreprise</em>
            </Option>
            {Array.isArray(data) && data.length > 0 && data.map((uneData,index) => (
              <Option key={index} value={uneData.siren}>
                {uneData.nom}
              </Option>
            ))}
          </Select>
            <Button onClick={(e) => saveSuppr(e)} color="primary" variant="outlined">
              Enregistrer la suppression
            </Button>



          </div>
          
        </Sheet>
    </Modal>
)}

export default ModalSuppression;
