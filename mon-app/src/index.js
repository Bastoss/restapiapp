import React from "react";
import { createRoot } from "react-dom/client";
import "./index.css";
import Home from "./pages/Home";
import About from "./pages/About";
import Layout from "./layouts/Layout";
import reportWebVitals from "./reportWebVitals";
import { DataProvider } from "./context/DataContext";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

const rootElement = document.getElementById("root");

const app = (
  <React.StrictMode>
    <Router>
      <DataProvider>
        <Layout>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
          </Routes>
        </Layout>
      </DataProvider>
    </Router>
  </React.StrictMode>
);

// Utilisez createRoot depuis "react-dom/client"
const root = createRoot(rootElement);
root.render(app);

// Mesurer les performances si nécessaire
reportWebVitals();
