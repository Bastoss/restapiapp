import axios from "axios";
import React, { useEffect, useState } from "react";
import Input from "@mui/joy/Input";
import Button from "@mui/joy/Button";
import Card from "@mui/joy/Card";
import Modal from "@mui/joy/Modal";
import ModalClose from "@mui/joy/ModalClose";
import ModalVueEntreprise from "../modals/ModalVueEntreprise";
import Sheet from "@mui/joy/Sheet";
import Typography from "@mui/joy/Typography";
import { useData } from "../context/DataContext";

function About() {
  const [data, setData] = useState([]);
  const [uneEntreprise, setUneEntreprise] = useState();
  const [error, setError] = useState();
  const [open, setOpen] = useState(false);
  const { data2, updateData } = useData();
  const [ouvrirDetail, setOuvrirDetail] = useState();
  const [entreprise, setEntreprise] = useState("");
  const [appelApiEntreprise, setAppelApiEntreprise] = useState("");

  const test = (siren) => {
    setAppelApiEntreprise(true);
    setOuvrirDetail(true);
    setEntreprise(siren);
  };

  useEffect(() => {
    axios
      .get("http://127.0.0.1:8000/api/api-ouverte-ent-liste.php", {
        headers: {
          Accept: "application/json",
        },
      })
      .then((data) => {
        console.log(data);
        setData(data.data);
      });
  }, []);

  useEffect(() => {
    console.log("Data2 de About.js", data2);
    setData(data2);
  }, [updateData]);

  const envoiDonnees = (e) => {
    const inputs = e.target.parentElement.getElementsByTagName("input");

    const siren = inputs[0].value;
    const raison_sociale = inputs[1].value;
    const num = inputs[2].value;
    const voie = inputs[3].value;
    const code_postale = inputs[4].value;
    const ville = inputs[5].value;
    const latitude = inputs[6].value;
    const longitude = inputs[7].value;

    const entreprise = {
      siren: siren,
      raison_sociale: raison_sociale,
      adresse: {
        num: num,
        voie: voie,
        code_postale: code_postale,
        ville: ville,
        gps: {
          latitude: latitude,
          longitude: longitude,
        },
      },
    };

    axios
      .post("http://127.0.0.1:8000/api/api-ouverte-entreprise", entreprise)
      .then((dataEnCours) => {
        setOpen(false);
        setData((prevData) => [...prevData, dataEnCours.data[0]]);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const chercherEntreprise = (e) => {
    const valeurInput =
      e.target.parentElement.getElementsByTagName("input")[0].value;
    axios
      .get("http://127.0.0.1:8000/api/api-ouverte-ent.php", {
        params: {
          siren: valeurInput,
        },
      })
      .then((data) => {
        console.log(data);
        setUneEntreprise(data.data);
        setError("");
      })
      .catch(function (error) {
        console.log(error);
        if (error.response.status === 404) {
          setError(error.response.data.error);
          setUneEntreprise("");
        }
      });
  };

  return (
    <div className="w-11/12">
      <div
        style={{
          textAlign: "center",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          marginBottom: "25px",
        }}
      >
        <div className="flex justify-between	w-full">
          <div className="w-6/12">
            <Card
              color="danger"
              orientation="vertical"
              size="lg"
              variant="soft"
            >
              <h1 className="text-4xl mb-5">
                Liste des entreprises enregistrés :{" "}
              </h1>

              <ul>
                {Array.isArray(data) && data.length > 0 ? (
                  data.map((uneData) => (
                    <li onClick={() => test(uneData)} key={uneData.id}>
                      {uneData.nom}
                    </li>
                  ))
                ) : (
                  <p>Pas d'entreprise enregistrée</p>
                )}
              </ul>
              <Button
                variant="outlined"
                color="danger"
                onClick={() => setOpen(true)}
              >
                Créer une entreprise
              </Button>
            </Card>
          </div>
          <div className="text-center flex flex-col items-center w-5/12">
            <Card
              color="primary"
              orientation="vertical"
              size="lg"
              variant="soft"
              className="w-full"
            >
              <h1 className="text-4xl mb-5">Rechercher une entreprise</h1>
              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "center",
                }}
              >
                <Input
                  style={{ width: "300px" }}
                  color="primary"
                  placeholder="Entrer le n° de SIREN"
                  size="lg"
                  variant="outlined"
                />
                <Button
                  color="primary"
                  onClick={chercherEntreprise}
                  variant="outlined"
                >
                  Envoyer
                </Button>
              </div>

              <div>
                {error !== "" ? (
                  <p className="text-red-600 mt-5">{error}</p>
                ) : (
                  <div className="mt-5">
                    <p className="text-xl">SIREN : {uneEntreprise.siren}</p>
                    <p className="text-xl">Nom : {uneEntreprise.nom}</p>
                    <p className="text-xl">
                      Raison Sociale : {uneEntreprise.raisonSociale}
                    </p>
                    <p className="text-xl">SIRET : {uneEntreprise.siret}</p>
                    <p className="text-xl">Adresse : {uneEntreprise.adresse}</p>
                  </div>
                )}
              </div>
            </Card>
          </div>
        </div>

        <ModalVueEntreprise
          ouvrirModif={ouvrirDetail}
          setOuvrirModif={setOuvrirDetail}
          entreprise={entreprise}
          appelApiEntreprise={appelApiEntreprise}
        />

        <Modal
          aria-labelledby="modal-title"
          aria-describedby="modal-desc"
          open={open}
          onClose={() => setOpen(false)}
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Sheet
            variant="outlined"
            sx={{
              maxWidth: 500,
              borderRadius: "md",
              p: 3,
              boxShadow: "lg",
            }}
          >
            <ModalClose variant="plain" sx={{ m: 1 }} />
            <Typography
              component="h2"
              id="modal-title"
              level="h4"
              textColor="inherit"
              fontWeight="lg"
              mb={1}
            >
              Créer une entreprise
            </Typography>
            <Typography id="modal-desc" textColor="text.tertiary">
              Cette modal permet de créer une entreprise en base de données.
            </Typography>
            <div className="flex flex-col items-center gap-10 mt-10">
              <Input
                className="w-11/12"
                color="primary"
                placeholder="SIREN"
                size="lg"
                variant="outlined"
              />
              <Input
                className="w-11/12"
                color="primary"
                placeholder="Raison sociale / Nom"
                size="lg"
                variant="outlined"
              />
              <div className="flex flex-col items-center">
                <div className="flex justify-center gap-5">
                  <Input
                    className="w-4/12"
                    color="primary"
                    placeholder="N° de voie"
                    size="lg"
                    variant="outlined"
                  />
                  <Input
                    className="w-6/12"
                    color="primary"
                    placeholder="Voie"
                    size="lg"
                    variant="outlined"
                  />
                </div>
                <div className="flex justify-center gap-5 mt-5">
                  <Input
                    className="w-4/12"
                    color="primary"
                    placeholder="Code Postale"
                    size="lg"
                    variant="outlined"
                  />
                  <Input
                    className="w-6/12"
                    color="primary"
                    placeholder="Ville"
                    size="lg"
                    variant="outlined"
                  />
                </div>

                <div className="flex justify-center gap-5 mt-5">
                  <Input
                    className="w-4/12"
                    color="primary"
                    placeholder="Latitude"
                    size="lg"
                    variant="outlined"
                  />
                  <Input
                    className="w-6/12"
                    color="primary"
                    placeholder="Longitude"
                    size="lg"
                    variant="outlined"
                  />
                </div>
              </div>

              <Button color="primary" onClick={envoiDonnees} variant="outlined">
                Soumettre
              </Button>
            </div>
          </Sheet>
        </Modal>
      </div>
    </div>
  );
}

export default About;
