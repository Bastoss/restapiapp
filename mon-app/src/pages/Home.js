
import PropTypes from 'prop-types';
import Box from '@mui/joy/Box';
import Table from '@mui/joy/Table';
import Typography from '@mui/joy/Typography';
import Sheet from '@mui/joy/Sheet';
import Checkbox from '@mui/joy/Checkbox';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import IconButton from '@mui/joy/IconButton';
import Link from '@mui/joy/Link';
import Tooltip from '@mui/joy/Tooltip';
import Select from '@mui/joy/Select';
import Option from '@mui/joy/Option';
// import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import { visuallyHidden } from '@mui/utils';
import Input from '@mui/joy/Input';
import Button from '@mui/joy/Button';
// import AddCircleIcon from '@mui/icons-material/AddCircle';
import React, { useEffect, useState } from "react";
import axios from "axios";
import DownloadForOfflineIcon from '@mui/icons-material/DownloadForOffline';






function labelDisplayedRows({ from, to, count }) {
  return `${from}–${to} of ${count !== -1 ? count : `more than ${to}`}`;
}


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nom',
  },
  {
    id: 'calories',
    numeric: true,
    disablePadding: false,
    label: 'Adresse',
  },
  {
    id: 'fat',
    numeric: true,
    disablePadding: false,
    label: 'Raison sociale',
  },
  {
    id: 'carbs',
    numeric: true,
    disablePadding: false,
    label: 'N°SIREN',
  },
  {
    id: 'protein',
    numeric: true,
    disablePadding: false,
    label: 'N°SIRET',
  },
];



function EnhancedTableHead(props) {




  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
    props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  console.log(rowCount);
  console.log(numSelected);

  return (
    <thead>
      <tr>
        <th>
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            slotProps={{
              input: {
                'aria-label': 'select all desserts',
              },
            }}
            sx={{ verticalAlign: 'sub' }}
          />
        </th>
        {headCells.map((headCell) => {
          const active = orderBy === headCell.id;
          return (
            <th
              key={headCell.id}
              aria-sort={
                active ? { asc: 'ascending', desc: 'descending' }[order] : undefined
              }
            >
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <Link
                underline="none"
                color="neutral"
                textColor={active ? 'primary.plainColor' : undefined}
                component="button"
                onClick={createSortHandler(headCell.id)}
                fontWeight="lg"
                startDecorator={
                  headCell.numeric ? (
                    <ArrowDownwardIcon sx={{ opacity: active ? 1 : 0 }} />
                  ) : null
                }
                endDecorator={
                  !headCell.numeric ? (
                    <ArrowDownwardIcon sx={{ opacity: active ? 1 : 0 }} />
                  ) : null
                }
                sx={{
                  '& svg': {
                    transition: '0.2s',
                    transform:
                      active && order === 'desc' ? 'rotate(0deg)' : 'rotate(180deg)',
                  },
                  '&:hover': { '& svg': { opacity: 1 } },
                }}
              >
                {headCell.label}
                {active ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </Box>
                ) : null}
              </Link>
            </th>
          );
        })}
      </tr>
    </thead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};




const downloadEnterprise = (rowsSend, setSelected,setRowsSend) => {
  console.log(rowsSend[0]);

  axios
    .post(
        'http://127.0.0.1:8000/api/entreprises',
          rowsSend[0],
          {
            headers: {
                'content-type': 'application/json',
            },
        }
    )
    .then(function (response) {
      console.log(response.data)
    })
    .catch(function (error) {
        console.error(error);
    });


    setSelected([]);
    setRowsSend([]);


}


function EnhancedTableToolbar(props) {
  const { numSelected,rowsSend,setSelected,setRowsSend } = props;
  
  

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        py: 1,
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: 'background.level1',
        }),
        borderTopLeftRadius: 'var(--unstable_actionRadius)',
        borderTopRightRadius: 'var(--unstable_actionRadius)',
      }}
    >
      {numSelected > 0 ? (
        <Typography sx={{ flex: '1 1 100%' }} component="div">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          level="body-lg"
          sx={{ flex: '1 1 100%' }}
          id="tableTitle"
          component="div"
        >
          Résultats
        </Typography>
      )}

{numSelected > 0 ? (
        <Tooltip title="ajouter">
          <IconButton size="sm" color="primary" variant="solid">
            <DownloadForOfflineIcon onClick={() => downloadEnterprise(rowsSend,setSelected,setRowsSend)} />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton size="sm" variant="outlined" color="neutral">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}

      
    </Box>

    
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};



export default function TableSortAndSelection() {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  let [rowsSend,setRowsSend] = useState([]);

  const [data, setData] = useState([]);

  // useEffect(() => {
  //   axios.get("http://127.0.0.1:8000/api/entreprises").then((data) => {
  //     console.log(data);
  //     setData(data?.data);
  //   });
  // },[]);

  const handleChange = (e) => {
    const valeurInput = e.target.parentElement.getElementsByTagName('input')[0].value;
    
    console.log(valeurInput);
    axios
    .get(
        'http://127.0.0.1:8000/api/apientreprises',
          {
            params: {
              "nom": valeurInput
            }
          },
          {
            headers: {
                'content-type': 'application/json',
            },
        }
    )
    .then(function (response) {
      setData(response.data);
      setSelected([]);
      setRowsSend([])
    })
    .catch(function (error) {
        console.error(error);
    });
    console.log(data)


  }


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if(selected.length <= 0){

      console.log("aucune case de select");
      const newSelected = data.map((n) => n.siren);
      console.log(data);
      setSelected(newSelected); 
    }
    else{
      setSelected([]);
      console.log("Au moins une de selectionné");
    }
  };

  const handleClick = (event, row) => {

    const selectedIndex = selected.indexOf(row.siren);

    let newSelected = [];
    let newSend = [...rowsSend];

    console.log("Selected index : ", selectedIndex);
    console.log("Selected length -1 : ", selected.length - 1);
    console.log("Rows Send : ",rowsSend);
    


    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row.siren);
      newSend = newSend.concat([row]);
    } 
    else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      newSend = newSend.slice(1);
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      newSend = newSend.slice(0,-1);
      
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      let newSend2 = [];
      newSend2 = newSend2.concat(newSend.slice(selectedIndex + 1),newSend.slice(0,selectedIndex))
      newSend = newSend2;
    }
    setRowsSend(newSend);
    setSelected(newSelected);
  };

  const handleChangePage = (newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event, newValue) => {
    setRowsPerPage(parseInt(newValue.toString(), 10));
    setPage(0);
  };

  const getLabelDisplayedRowsTo = () => {
    if (data.length === -1) {
      return (page + 1) * rowsPerPage;
    }
    return rowsPerPage === -1
      ? data.length
      : Math.min(data.length, (page + 1) * rowsPerPage);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;


  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;



  return (
    <div>
      <div style={{textAlign:"center", display:"flex", alignItems:"center", flexDirection:"column", marginBottom:"25px"}}>
        <h1 className='text-3xl mb-10'>Page d'accueil</h1>
        <div style={{width: "700px", display:"flex", flexDirection:"column" , gap:"10px", justifyContent:"center", alignItems:'center'}}>
            <Input style={{width:"300px"}} color="primary" placeholder="Rechercher une entreprise" size="lg" variant="outlined" />
            <Button color="primary" onClick={handleChange} variant="outlined">
              Envoyer
            </Button>
        </div>
      </div>
      
      <div style={{width:"100%", display:"flex", justifyContent:"center"}}>
        <Sheet
          variant="outlined"
          sx={{ width: '80%', boxShadow: 'sm', borderRadius: 'sm' }}
        >
          <EnhancedTableToolbar numSelected={selected.length} rowsSend={rowsSend} setRowsSend={setRowsSend}  setSelected={setSelected}/>
          <Table
            aria-labelledby="tableTitle"
            hoverRow
            sx={{
              '--TableCell-headBackground': 'transparent',
              '--TableCell-selectedBackground': (theme) =>
                theme.vars.palette.success.softBg,
              '& thead th:nth-child(1)': {
                width: '40px',
              },
              '& thead th:nth-child(2)': {
                width: '30%',
              },
              '& tr > *:nth-child(n+3)': { textAlign: 'right' },
            }}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={data.length}
            />
            <tbody>
              {stableSort(data, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.siren);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <tr
                      onClick={(event) => handleClick(event, row)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.siren}
                      // selected={isItemSelected}
                      style={
                        isItemSelected
                          ? {
                              '--TableCell-dataBackground':
                                'var(--TableCell-selectedBackground)',
                              '--TableCell-headBackground':
                                'var(--TableCell-selectedBackground)',
                            }
                          : {}
                      }
                    >
                      <th scope="row">
                        <Checkbox
                          checked={isItemSelected}
                          slotProps={{
                            input: {
                              'aria-labelledby': labelId,
                            },
                          }}
                          sx={{ verticalAlign: 'top' }}
                        />
                      </th>
                      <th id={labelId} scope="row">
                        {row.nom}
                      </th>
                      <td>{row.adresse}</td>
                      <td>{row.raisonSociale}</td>
                      <td>{row.siren}</td>
                      <td>{row.siret}</td>
                    </tr>
                  );
                })}
              {emptyRows > 0 && (
                <tr
                  style={{
                    height: `calc(${emptyRows} * 40px)`,
                    '--TableRow-hoverBackground': 'transparent',
                  }}
                >
                  <td colSpan={6} aria-hidden />
                </tr>
              )}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan={6}>
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      gap: 2,
                      justifyContent: 'flex-end',
                    }}
                  >
                    <FormControl orientation="horizontal" size="sm">
                      <FormLabel>Ligne par page:</FormLabel>
                      <Select onChange={handleChangeRowsPerPage} value={rowsPerPage}>
                        <Option value={5}>5</Option>
                        <Option value={10}>10</Option>
                        <Option value={25}>25</Option>
                      </Select>
                    </FormControl>
                    <Typography textAlign="center" sx={{ minWidth: 80 }}>
                      {labelDisplayedRows({
                        from: data.length === 0 ? 0 : page * rowsPerPage + 1,
                        to: getLabelDisplayedRowsTo(),
                        count: data.length === -1 ? -1 : data.length,
                      })}
                    </Typography>
                    <Box sx={{ display: 'flex', gap: 1 }}>
                      <IconButton
                        size="sm"
                        color="neutral"
                        variant="outlined"
                        disabled={page === 0}
                        onClick={() => handleChangePage(page - 1)}
                        sx={{ bgcolor: 'background.surface' }}
                      >
                        <KeyboardArrowLeftIcon />
                      </IconButton>
                      <IconButton
                        size="sm"
                        color="neutral"
                        variant="outlined"
                        disabled={
                          data.length !== -1
                            ? page >= Math.ceil(data.length / rowsPerPage) - 1
                            : false
                        }
                        onClick={() => handleChangePage(page + 1)}
                        sx={{ bgcolor: 'background.surface' }}
                      >
                        <KeyboardArrowRightIcon />
                      </IconButton>
                    </Box>
                  </Box>
                </td>
              </tr>
            </tfoot>
          </Table>
        </Sheet>
      </div>

    </div>
  );
}