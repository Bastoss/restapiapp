import axios from 'axios';
import { createContext, useContext, useState } from 'react';

const DataContext = createContext();

export const DataProvider = ({ children }) => {
  const [data2, setData] = useState([]);

  const updateData = () => {

    console.log(data2);

    axios.get("http://127.0.0.1:8000/api/api-ouverte-ent-liste.php", {
        headers: {
          'Accept': 'application/json'
      }
      }).then((data) => {
        setData(data.data)
      }); 

    console.log("Data updated");
  };

  return (
    <DataContext.Provider value={{ data2, updateData }}>
      {children}
    </DataContext.Provider>
  );
};

export const useData = () => {
  return useContext(DataContext);
};