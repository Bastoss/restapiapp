import Navbar from '../components/Navbar';
import About from '../pages/About';
import ModalModif from '../modals/ModalModif';
import ModalSuppression from '../modals/ModalSuppression';
import Modal from '@mui/joy/Modal';
import ModalClose from '@mui/joy/ModalClose';
import Sheet from '@mui/joy/Sheet';
import Typography from '@mui/joy/Typography';
import Input from '@mui/joy/Input';
import Button from '@mui/joy/Button';
import Key from '@mui/icons-material/Key';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import React, {useState } from 'react';
import axios from 'axios';
import btoa from 'btoa-lite';
import Box from '@mui/joy/Box';
import Alert from '@mui/joy/Alert';
import IconButton from '@mui/joy/IconButton'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import Snackbar from '@mui/joy/Snackbar';



const Layout = ({ children }) => {

  const [open, setOpen] = useState(false);
  const [ouvrirSuppr, setOuvrirSuppr] = useState(false);
  const [ouvrirModif, setOuvrirModif] = useState(false);
  const [openCreation, setOpenCreation] = useState(false);
  const [estConnectee,setEstConnectee] = useState(localStorage.getItem('estConnectee') === 'true');
  const [error,setError] = useState('');
  const [alert,setAlert] = useState(false);

  

  const handleDataFromChild = (data) => {
    setOpen(data);
    setOpenCreation(false);
  };

  const redirectionCreation = () => {
    setOpenCreation(true);
    setOpen(false);
  }

  const compte = (action, e) => {
    if (action === "connexion") {
      const username = e.target.parentElement.getElementsByTagName('input')[0].value;
      const mdp = e.target.parentElement.getElementsByTagName('input')[1].value;

      const token = `${username}:${mdp}`;
      const encodedToken = btoa(token);
      const session_url = 'http://127.0.0.1:8000/compte/login';
  

      var config = {
        method: 'post',
        url: session_url,
        headers: { 'Authorization': 'Basic '+ encodedToken }
      };

  
      axios(config)
      .then(function (response) {
        console.log("sa passe");
        localStorage.setItem('estConnectee', 'true');
        setEstConnectee(true);
        setOpen(false);
        setError('');
      })
      .catch(function (error) {
        console.log(error);
        setError(error.response.data.error)
      });

    } else if (action === "creation") {

      const username = e.target.parentElement.getElementsByTagName('input')[0].value;
      const mdp = e.target.parentElement.getElementsByTagName('input')[1].value;

  
      const token = `${username}:${mdp}`;
      const encodedToken = btoa(token);
      const session_url = 'http://127.0.0.1:8000/compte/create_account.php';
  
      var config = {
        method: 'post',
        url: session_url,
        data: {
          username: username,
          password: mdp
        }
      };
  
      axios(config)
      .then(function (response) {
        handleDataFromChild(true);
        setAlert(true);
        setError('');
      })
      .catch(function (error) {
        setError(error.response.data.error)
      });
    }
  };
  

  return (
    <div className='flex flex-col items-center'>
      <Navbar setOuvrirModif={setOuvrirModif} setOuvrirSuppr={setOuvrirSuppr}  sendOpen={handleDataFromChild} estConnectee={estConnectee} setEstConnectee={setEstConnectee}/>
      {children}
      <ModalModif setOuvrirModif={setOuvrirModif}  ouvrirModif={ouvrirModif}/>
      <ModalSuppression setOuvrirSuppr={setOuvrirSuppr}  ouvrirSuppr={ouvrirSuppr}/>
      <Modal
        aria-labelledby="modal-title"
        aria-describedby="modal-desc"
        open={open}
        onClose={() => setOpen(false)}
        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      >
        <Sheet
          variant="outlined"
          sx={{
            maxWidth: 500,
            borderRadius: 'md',
            p: 3,
            boxShadow: 'lg',
          }}
        >
        <Snackbar
        autoHideDuration={3000}
        variant="solid"
        color="success"
        size="lg"
        startDecorator={<CheckCircleIcon />}
        invertedColors
        open={alert}
        onClose={() => setAlert(false)}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        
      >
         <div>
            <div>Succès</div>
            <Typography level="body-sm" color="success">
              Votre compte à bien été crée !
            </Typography>
          </div>
      </Snackbar>
          <ModalClose variant="plain" sx={{ m: 1 }} />
          <Typography
            component="h2"
            id="modal-title"
            level="h4"
            textColor="inherit"
            fontWeight="lg"
            mb={1}
          >
           Connexion
          </Typography>
          <Typography id="modal-desc" textColor="text.tertiary">
            Vous n'avez pas d'identifiant ? <span className='text-sky-500 underline cursor-pointer' onClick={redirectionCreation}>Cliquez ici</span> pour créer un compte.
          </Typography>
          <div className='flex flex-col items-center gap-4 mt-10'>
            <Input className='w-11/12' color="primary" placeholder="Identifiant" size="lg" variant="outlined" startDecorator={<AccountCircleIcon />}/>
            <Input className='w-11/12 mb-5' color="primary" placeholder="Mot de passe" size="lg" variant="outlined" type="password" startDecorator={<Key />}/>
          {error !== "" ?  ( <p className='text-red-500 font-bold mb-5 italic'>{error}</p>) : "" }
            <Button onClick={(e) => compte("connexion",e)} color="primary" variant="outlined">
              Se connecter
            </Button>



          </div>
          
        </Sheet>
      </Modal>

      <Modal
        aria-labelledby="modal-title"
        aria-describedby="modal-desc"
        open={openCreation}
        onClose={() => setOpenCreation(false)}
        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      >
        <Sheet
          variant="outlined"
          sx={{
            maxWidth: 500,
            borderRadius: 'md',
            p: 3,
            boxShadow: 'lg',
          }}
        >
          <ModalClose variant="plain" sx={{ m: 1 }} />
          <Typography
            component="h2"
            id="modal-title"
            level="h4"
            textColor="inherit"
            fontWeight="lg"
            mb={1}
          >
           Création de compte
          </Typography>
          <Typography id="modal-desc" textColor="text.tertiary">
            Vous avez déjà un compte ? <span className='text-sky-500 underline cursor-pointer	' onClick={handleDataFromChild}>Cliquez ici</span> pour vous connecter.
          </Typography>
          <div className='flex flex-col items-center gap-4 mt-10'>
            <Input className='w-11/12' color="primary" placeholder="Identifiant" size="lg" variant="outlined" startDecorator={<AccountCircleIcon />}/>
            <Input className='w-11/12 mb-5' color="primary" placeholder="Mot de passe" size="lg" variant="outlined" type="password" startDecorator={<Key />}/>
            <Button onClick={(e) => compte("creation",e)} color="primary" variant="outlined">
              Créer un compte
            </Button>



          </div>
          
        </Sheet>
      </Modal>
    </div>
  );
};

export default Layout;